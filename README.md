### Semestrální práce NI-MVI: Modelování témat pomocí sítě BERT
Rozhodla jsem se, že ve své semestrální práci implementuji modelování témat pomocí BERTu. Síť BERT mi slouží k získání slovních embeddingů, které jsou poté podrobeny redukci dimenzionality a shlukování, čímž jsou extrahována jednotlivá témata.

Data mám odsud: [ArXiv dataset](https://www.kaggle.com/datasets/Cornell-University/arxiv). Odsud je lze také stáhnout; jsou ve formátu JSON. Ve své práci používám názvy článků z tohoto datasetu a zpracovávám jejich abstrakty. Z důvodu nedostatečné výpočetní kapacity je zpracovávána jen jejich nevelká část.

Jako zdroje mi slouží tyto články:
- [BERT](https://arxiv.org/abs/1810.04805)
- [Sentence-BERT](https://arxiv.org/abs/1908.10084)
- [Topic Modeling with LDA and BERT](https://ieeexplore.ieee.org/abstract/document/9558988?casa_token=p6VJJHZsFYkAAAAA:WJnqnkWvvIpBhwYC2nUx-KMmoIwGEdr1FUyjIOhwqK3Q5P7TBVthoC3KIuCOroVraRYtJr-XSQ)

Výsledkem mé práce jsou dva Jupyter notebooky. V jednom pracuji s předtrénovaným modelem BERTu a ve druhém s modelem, který sama trénuji přímo na svých datech.

(Poznámka: V Gitlabu se přinejmenším mně zobrazují ony notebooky divně - chybí komentáře apod. Po stažení notebooku a jeho nahrání do Colabu by vše mělo být v pořádku.)
